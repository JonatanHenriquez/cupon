<?php

    namespace AppBundle\Controller;

    use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    use Symfony\Component\HttpFoundation\Request;

    class ExtranetController extends Controller
    {

        /**
         * @Route("/extranet/login", name="extranet_login")
         */
        public function extranetLogin(Request $request){
            $error_ = null;
            if ($this->get("session")->has("_security_default"))
                return $this->redirectToRoute("portadaExtranet");

            $authenticationUtils = $this->get('security.authentication_utils');

            // get the login error if there is one
            $error = $authenticationUtils->getLastAuthenticationError();
            if ($error) {
                if ($error->getMessageKey() == "Account is disabled.")
                    $error_ = "Usuario deshabilitado.";
                if ($error->getMessageKey() == "Account is locked.")
                    $error_ = "Usuario bloqueado.";
                if ($error->getMessageKey() == "Invalid credentials.")
                    $error_ = "Credenciales incorrectas.";
            }

            // last username entered by the user
            $lastUsername = $authenticationUtils->getLastUsername();
            return $this->render(
                'AppBundle:extranet:loginExtranet.html.twig' ,
                array(
                    // last username entered by the user
                    'last_username' => $lastUsername ,
                    'error' => $error_ ,
                )
            );

        }

        /**
         * @Route("/extranet/portada/", name="portadaExtranet")
         */
        public function portadaExtranetAction()
        {
            //pagina q me permitira ver todas las ofertas que he publicado con la opcion de editar aquellas
            //aun que no han sido revisadas y publicadas por el admin del sistema, si son revisadas es decir
            //que ya se publicaron
            return $this->render('AppBundle:extranet:extranet.html.twig');
        }

        /**
         * @Route("/extranet/oferta/nueva", name="nuevaOfertaExtranet")
         */
        public function nuevaOfertaExtranetAction()
        {

        }

        /**
         * @Route("/extranet/oferta/editar/{id}", name="editarOfertaExtranet")
         */
        public function editarOfertaExtranetAction($id)
        {

        }

        /**
         * @Route("/extranet/oferta/ventas/{id}", name="ventasOfertaExtranet")
         */
        public function ventasOfertaExtranetAction($id)
        {

        }

        /**
         * @Route("/extranet/perfil", name="perfilExtranet")
         */
        public function perfilExtranetAction($id)
        {

        }



    }
